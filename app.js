var express = require('express'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    errorHandler = require('error-handler'),
    morgan = require('morgan'),
    routes = require('./routes'),
    api = require('./routes/api'),
    http = require('http'),
    mysql = require('mysql'),
    path = require('path');

var app = express();

/**
 * Configuration
 */

// all environments
app.set('port', process.env.PORT || 8000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(morgan('dev'));
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));

var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'zleaksrocks123',
    database : 'zleaks_db'
});

// database connection
connection.connect(function(err){
    if(!err) {
        console.log("Database is connected...");    
    } else {
        console.log("Error connecting database...");    
    }
});

// Environment Variables

var hasLeak = false;
var changeValve = false;
var closedValve = false;
var hasResponseLeak = false;

app.get('/', routes.index);

// API

// Time API 
// This route returns the serve's time
app.get('/api/time', api.time);

// Sensor Data API
// This route saves in a table the data got from sensor 
// in hardware system
app.post('/api/sensordata', function (req, res) {
    data = req.body;
    
    data.pressure = data.pressure.split(',');
    data.temp = data.temp.split(',');
    data.humidity = data.humidity.split(',');
    data.flux = data.flux.split(',');

    var data2;
    for(var i = 0; i < data.pressure.length; i++) {
        data2 = {
                    'device': data.device, 
                    'pressure': data.pressure[i],
                    'temp' : data.temp[i],
                    'humidity' : data.humidity[i],
                    'flux' : data.flux[i],
                    'time' : data.time
                };

        connection.query('insert into tb_instwater SET ?', data2,  function selectCb(err, results, fields) {
            if (err) throw err;
        });
    }

    res.json({
        status: 'Ok'
    });
});

app.get('/api/time1', function (req, res) {
    var current_hour = date.getHours();
    var current_minute = date.getMinutes();
    var current_second = date.getSeconds();
    var current_day = date.getDay();
    var current_day = date.getDate();
    var current_month = date.getMonth() + 1;
    var current_year = date.getFullYear();
    var fullTime = JSON.stringify({year: current_year, day: current_day, month: current_month, hour : current_hour, minute : current_minute, second : current_second});

    res.send(fullTime);
});

// This route stores waste in the database
app.post('/api/waste', function (req, res) {
    data = req.body;    
    console.info(data);

    connection.query('INSERT into tb_waterdatas SET ?', data,  function selectCb(err, results, fields) {
        if (err) throw err;
    });

    res.json({ status: 'Ok' });
});

// This route returns the data stored about waste
app.get('/api/waste', function (req, res) {

    var data2 = "";
    var queryString = 'SELECT * FROM tb_instwater ORDER BY id DESC LIMIT 1 ';
    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;
        for (var i in rows) {
           //console.log(rows[0].flux);
	       data2 =rows[0].flux;
       	   res.json({status: 'Ok', data : data2}); 
	       //console.dir(i + "-" + data2);
	    }
    });
});

// This route returns the data stored about the last 10 measures about waste
app.get('/api/wastesgraph', function (req, res) {

    var data2 = [];
    var queryString = 'SELECT * FROM tb_waterdata ORDER BY id DESC LIMIT 10 ';
    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;


        for (var i in rows) {
            var period = JSON.stringify(rows[i].time);
            var res2 = String(period.split("T")[1].split(".")[0]);

            //console.dir(res2);
            //console.dir(typeof res2);

            data2.push({
            	period: res2,
           		consumo: rows[i].waste
        	});
	    }
        data2.reverse();
       	res.json({status: 'Ok', data : data2}); 
    });
});

app.get('/api/daywaste', function (req, res) {

    var data2 = "";
    var queryString = 'SELECT SUM(`waste`) AS sum FROM tb_waterdatas ';
    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;
        for (var i in rows) {
           data2 = rows[0];
           res.json({status: 'Ok', data : data2}); 
        }
    });
});

// Leak API
// This route is used by hardware system to inform a detected leak
app.post('/api/leak', function (req, res) {
    hasLeak = !hasLeak;
    res.json({ status: 'Ok' });
});

// This route returns the leak states
app.get('/api/leak', function (req, res) {
    res.json({ leak: hasLeak });
});

//
app.post('/api/valve', function (req, res) {
    changeValve = !changeValve;

    res.json({
        status: 'Ok'
    });
});

// 
app.get('/api/valve', function (req, res) {
    res.send(changeValve);
    changeValve = false;
    closedValve = !closedValve;
});

app.get('*', routes.index);

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
