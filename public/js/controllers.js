'use strict';

/* Controllers */

angular.module('myApp.controllers', []).
  controller('AppCtrl', function ($scope, $http) {
    
    $scope.button = "Fechar Fluxo";

    var notifiedLeak = false;

    var updateButton = function()
    {
      $http({
        method: 'POST',
        url: '/api/valve'
      }).
      success(function (data, status, headers, config) {

        if($scope.button === "Fechar Fluxo") {
          $scope.button = "Abrir Fluxo";
        } else if ($scope.button === "Abrir Fluxo") {
          $scope.button = "Fechar Fluxo";
        }
      });
    }; 

    var updateLeakStatus = function()
    {
      $http({
        method: 'GET',
        url: '/api/leak'
      }).
      success(function (data, status, headers, config) {
        //console.dir(data);
        if (data.leak && !notifiedLeak){
          confirm("Vazamento Detectado");
          confirm("Você costuma usar água nesse horário?");
          notifiedLeak = true;
        };
      });
    }

    var updateTime = function()
    {
      $http({
        method: 'GET',
        url: '/api/time'
      }).
      success(function (data, status, headers, config) {
        $scope.hour = data.hour;
        $scope.min = data.minute;
        $scope.sec = data.second;
      });
    }

    var updateNowLeak = function() {
        $http({
          method: 'GET',
          url: '/api/waste'
        }).
        success(function (data, status, headers, config) {
          console.log(data);
          $scope.waste = data.data.toFixed(2);
        });
    }

    var updateDayWaste = function() {
        $http({
          method: 'GET',
          url: '/api/daywaste'
        }).
        success(function (data, status, headers, config) {
          console.dir(data);
          $scope.daywaste = data.data.sum.toFixed(2);
        });
    }
    
    var graph = Morris.Area({
            element: 'morris-area-chart',
            data: [],
            xkey: 'period',
            ykeys: ['consumo'],
            labels: ['Consumo'],
            pointSize: 2,
            hideHover: 'auto',
            resize: true,
            parseTime: false
        });

    var updateWastesGraph = function() {
        $http({
          method: 'GET',
          url: '/api/wastesGraph'
        }).
        success(function (data, status, headers, config) {
          console.dir(data);
          graph.setData(data.data);
        });
    }

    var updateTasks = function()
    {
      updateDayWaste();
      updateNowLeak();
      updateWastesGraph();
      updateLeakStatus();
      updateTime();

      setTimeout(updateTasks, 3000);
    }

    updateDayWaste();
    updateNowLeak();
    updateWastesGraph();
    $scope.updateButton = updateButton;
    updateTasks();
   //updateGraph()

  });
