/*
 * Serve JSON to our AngularJS client
 */

exports.time = function (req, res) {

    var date = new Date();
    var current_hour = date.getHours();
    var current_minute = date.getMinutes();
    var current_second = date.getSeconds();
    var current_day = date.getDate();
    var current_month = date.getMonth() + 1;
    var current_year = date.getFullYear();
    var fullTime = JSON.stringify({year: current_year, day: current_day, month: current_month, hour : current_hour, minute : current_minute, second : current_second});

    res.send(fullTime);
};